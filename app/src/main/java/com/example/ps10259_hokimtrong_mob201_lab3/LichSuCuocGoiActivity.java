package com.example.ps10259_hokimtrong_mob201_lab3;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class LichSuCuocGoiActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_READ_CALL_LOG =7 ;
    ListView lvLichSuCuocGoi;
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lich_su_cuoc_goi);
        lvLichSuCuocGoi=findViewById(R.id.lvLichSuCuocGoi);
        requestReadCallLogPermission();
        showAllCallLog();
    }

    private void showAllCallLog(){
        String[] projection=new String[]{
                CallLog.Calls.DATE,
                CallLog.Calls.NUMBER,
                CallLog.Calls.DURATION
        };
        Cursor cursor=getContentResolver().query(CallLog.Calls.CONTENT_URI,
                projection,
                null,
                null,null);
        list=new ArrayList<>();
        while (cursor.moveToNext()){
            String s="";
            for(int i=0;i< cursor.getColumnCount();i++){
                s+= cursor.getString(i)+"--";
            }
            list.add(s);
        }
        cursor.close();
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
        lvLichSuCuocGoi.setAdapter(adapter);
    }

    private void requestReadCallLogPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(LichSuCuocGoiActivity.this,
                Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(LichSuCuocGoiActivity.this,
                    Manifest.permission.READ_CALL_LOG)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(LichSuCuocGoiActivity.this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        MY_PERMISSIONS_REQUEST_READ_CALL_LOG);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CALL_LOG: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
